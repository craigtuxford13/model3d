Model 3D Goxel Integration
==========================

This is a standalone implementation for [Goxel](https://goxel.xyz) to export voxel images in
[Model 3D format](https://gitlab.com/bztsrc/model3d/blob/master/docs/voxel_format.md).

Installation
------------

Download the [Goxel source](https://github.com/guillaumechereau/goxel), then copy this file into
the repository. Compile and enjoy!

Limitations
-----------

Since this is a non-SDK, standalone implementation, it does not support all M3D features. It does not save layer
names (or any strings as a matter of fact), and it is limited to 65535 voxel types and colored, full cubic voxels
only, and uses fixed 65536 x 65536 x 65536 dimensions (the format is capable of storing 2^32 x 2^32 x 2^32 btw).
These limitations make sense as Goxel itself does not handle non-cubic voxels nor voxel type names.
