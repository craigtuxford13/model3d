# ##### BEGIN MIT LICENSE BLOCK #####
#
# blender/io_scene_m3d.py
#
# Copyright (C) 2019 bzt (bztsrc@gitlab)
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# @brief Blender 2.80 Model 3D Exporter (and one day Importer too)
# https://gitlab.com/bztsrc/model3d
#
# ##### END MIT LICENSE BLOCK #####

# <pep8-80 compliant>

bl_info = {
    "name": "Model 3D (.m3d) format",
    "author": "bzt",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "description": "Export M3D",
    "wiki_url": "https://gitlab.com/bztsrc/model3d/blob/master/docs/m3d_format.md",
    "category": "Import-Export"}


# -----------------------------------------------------------------------------
# Import libraries
import bmesh
from os import getenv
from struct import pack, unpack
from mathutils import Matrix
from bpy_extras import io_utils, node_shader_utils
from bpy_extras.wm_utils.progress_report import (
    ProgressReport,
    ProgressReportSubstep,
)

# -----------------------------------------------------------------------------
#  Blender material property and M3D property type assignments
#  See https://gitlab.com/bztsrc/model3d/blob/master/docs/m3d_format.md section Materials)
mat_property_map = {
        #type  format   PrincipledBSDF property
          0: ["color",  "base_color"],
          1: ["gscale", "metallic"],
          2: ["gscale", "specular"],
          3: ["float",  "specular_tint"],
          4: ["//color","emissive"],        # not in BSDF?
          5: ["gscale", "transmission"],
          6: ["float",  "normalmap_strength"],
          7: ["float",  "alpha"],
          8: ["//byte", "illumination"],    # not in PBR at all
         64: ["float",  "roughness"],
         65: ["float",  "metallic"],
         66: ["//float","sheen"],           # not in BSDF?
         67: ["float",  "ior"],
        128: ["map",    "base_color_texture"],
        130: ["//map",  "specular_texture"],  # should work, but it does not
        133: ["map",    "transmission_texture"],
        134: ["map",    "normalmap_texture"],
        135: ["map",    "alpha_texture"],
        192: ["map",    "roughness_texture"],
        193: ["map",    "metallic_texture"],
        195: ["map",    "ior_texture"],
        }

# -----------------------------------------------------------------------------
# Load and parse a Model 3D file (unlike the exporter, this is WIP)
def read_m3d(context,
         filepath,
         report,
         global_matrix=None,
         ):

    # read in an index
    def getidx(data, fmt):
        idx = -1
        if fmt == 0:
            if len(data) > 0:
                idx = unpack("<B", data[0:1])[0]
                if idx > 253:
                    idx = idx - 256
                data = data[1:]
        elif fmt == 1:
            if len(data) > 1:
                idx = unpack("<H", data[0:2])[0]
                if idx > 65533:
                    idx = idx - 65536
                data = data[2:]
        elif fmt == 2:
            if len(data) > 3:
                idx = unpack("<I", data[0:4])[0]
                if idx > 4294967293:
                    idx = idx - 4294967296
                data = data[4:]
        return data, idx

    # read in a coordinate
    def getcrd(data, fmt):
        crd = 0
        if fmt == 0:
            crd = round(float(unpack("<b", data[0:1])[0]) / 127.0, 4)
            data = data[1:]
        elif fmt == 1:
            crd = round(float(unpack("<h", data[0:2])[0]) / 32767.0, 4)
            data = data[2:]
        elif fmt == 2:
            crd = unpack("<f", data[0:4])[0]
            data = data[4:]
        elif fmt == 3:
            crd = unpack("<d", data[0:8])[0]
            data = data[8:]
        if crd == -0.0:
            crd = 0.0
        return data, crd

    with ProgressReport(context.window_manager) as progress:

        ############ !!!!!!!!! Work In Progress !!!!!!!!!! ###############
        if global_matrix is None:
            global_matrix = axis_conversion(from_forward='Z', from_up='Y').to_4x4()

        # read in file
        f = open(filepath, 'rb')
        data = f.read()
        f.close()

        if len(data) < 8 or data[0:4] != b'3DMO':
            report({"ERROR"}, filepath + " is not a valid Model 3D file!")
            return {'FINISHED'}

        # skip over preview image chunk
        if data[8:12] == b'PRVW':
            length = unpack("<I", data[4:8])[0]
            data = data[length:]

        if data[8:12] != b'HEAD':
            import zlib
            data = zlib.decompress(data[8:])
        else:
            data = data[8:]

        if data[0:4] != b'HEAD':
            report({"ERROR"}, filepath + " is not a valid Model 3D file!")
            return {'FINISHED'}

        # load into Blender independent lists
        cmap = []
        tmap = []
        vrts = []
        faces = []
        shapes = []
        labels = []
        materials = []
        bones = []
        skins = []
        # parse model header and string table
        head = {
            'name':'', 'license':'', 'author':'', 'description':'',
            'scale':unpack("<f", data[8:12])[0],
            'vc_s':(data[12] >> 0) & 3, 'vi_s':(data[12] >> 2) & 3, 'si_s':(data[12] >> 4) & 3,
            'ci_s':(data[12] >> 6) & 3, 'ti_s':(data[13] >> 2) & 3, 'bi_s':(data[13] >> 2) & 3,
            'nb_s':(data[13] >> 4) & 3, 'sk_s':(data[13] >> 6) & 3, 'fc_s':(data[14] >> 0) & 3,
            'hi_s':(data[14] >> 2) & 3, 'fi_s':(data[14] >> 4) & 3,
        }
        length = unpack("<I", data[4:8])[0]
        chunk = data[16:length]
        data = data[length:]
        s = chunk.split(b'\000')
        head['name'] = str(s[0], 'utf-8')
        head['license'] = str(s[1], 'utf-8')
        head['author'] = str(s[2], 'utf-8')
        head['description'] = str(s[3], 'utf-8')
        strs = {0:''}
        i = len(s[0]) + len(s[1]) + len(s[2]) + len(s[3]) + 4
        for t in s[4:]:
            strs[i] = str(t, 'utf-8')
            i = i + len(t) + 1

        # decode chunks
        while len(data) > 4 and data[0:4] != b'OMD3':
            magic = data[0:4]
            length = unpack("<I", data[4:8])[0]
            if length < 8 or length > 16 * 1024 * 1024:
                report({"ERROR"}, str(magic,'utf-8') + " bad chunk in Model 3D file!")
                break
            chunk = data[8:length]

            # color map
            if magic == b'CMAP':
                while len(chunk) > 0:
                    cmap.append([
                        unpack("<B", chunk[0:1])[0] / 255.0,
                        unpack("<B", chunk[1:2])[0] / 255.0,
                        unpack("<B", chunk[2:3])[0] / 255.0,
                        unpack("<B", chunk[3:4])[0] / 255.0])
                    chunk = chunk[4:]

            # texture map
            elif magic == b'TMAP':
                while len(chunk) > 0:
                    u = v = 0.0
                    # don't use getcrd, because this is scaled to 255
                    if head['vc_s'] == 0:
                        u = float(unpack("<B", chunk[0:1])[0]) / 255.0
                        v = float(unpack("<B", chunk[1:2])[0]) / 255.0
                        chunk = chunk[2:]
                    elif head['vc_s'] == 1:
                        u = float(unpack("<H", chunk[0:2])[0]) / 65535.0
                        v = float(unpack("<H", chunk[2:4])[0]) / 65535.0
                        chunk = chunk[4:]
                    elif head['vc_s'] == 2:
                        u, v = unpack("<ff", chunk[0:8])
                        chunk = chunk[8:]
                    elif head['vc_s'] == 3:
                        u, v = unpack("<dd", chunk[0:16])
                        chunk = chunk[16:]
                    tmap.append([round(u, 4), round(v, 4)])

            # vertex list (for vertex and normals)
            elif magic == b'VRTS':
                while len(chunk) > 0:
                    chunk, x = getcrd(chunk, head['vc_s'])
                    chunk, y = getcrd(chunk, head['vc_s'])
                    chunk, z = getcrd(chunk, head['vc_s'])
                    chunk, w = getcrd(chunk, head['vc_s'])
                    chunk, c = getidx(chunk, head['ci_s'])
                    chunk, s = getidx(chunk, head['sk_s'])
                    if ci_s < 4 and c != -1:
                        c = cmap[c]
                    elif ci_s == 4:
                        c = [c[0:1] / 255.0, c[1:2] / 255.0, c[2:3] / 255.0, c[3:4] / 255.0]
                    vrts.append([x, y, z, w, c, s])

            # material chunk
            elif magic == b'MTRL':
                chunk, n = getidx(chunk, head['si_s'])
                props = {}
                while len(chunk) > 0:
                    typ = unpack("<B", chunk[0:1])[0]
                    chunk = chunk[1:]
                    try:
                        t = mat_property_map[typ];
                    except:
                        print("unknown property ", typ)
                        break;
                    if typ == 0:
                        chunk, c = getidx(chunk, head['ci_s'])
                        props["base_color"] = cmap[c]
                        props["alpha"] = cmap[c][3]
                    elif typ >= 128:
                        chunk, s = getidx(chunk, head['si_s'])
                        if t[1][0:2] != "//":
                            props[t[1]] = strs[s] + ".png"
                    elif t[0] == "gscale":
                        chunk, c = getidx(chunk, head['ci_s'])
                        props[t[1]] = round((cmap[c][0] + cmap[c][1] + cmap[c][2]) / 3, 4)
                    elif t[0] == "color":
                        chunk, c = getidx(chunk, head['ci_s'])
                        if ci_s < 4 and c != -1:
                            c = cmap[c]
                        elif ci_s == 4:
                            c = [c[0:1] / 255.0, c[1:2] / 255.0, c[2:3] / 255.0, c[3:4] / 255.0]
                        props[t[1]] = c
                    elif t[0] == "float":
                        chunk, c = getcrd(chunk, 2)
                        props[t[1]] = c
                    elif t[0] == "//map":
                        chunk = chunk[head['si_s']:]
                    elif t[0] == "//color":
                        chunk = chunk[head['ci_s']:]
                    elif t[0] == "//float":
                        chunk = chunk[4:]
                    elif t[0] == "//byte":
                        chunk = chunk[1:]
                    else:
                        break;
                materials.append([strs[n], props])

            # mesh (only triangles supported for now)
            elif magic == b'MESH':
                m = -1
                while len(chunk) > 0:
                    typ = unpack("<B", chunk[0:1])[0]
                    chunk = chunk[1:]
                    if typ == 0:
                        chunk, s = getidx(chunk, head['si_s'])
                        m = -1
                        if s != 0:
                            for i, mat in enumerate(materials):
                                if strs[s] == mat[0]:
                                    m = i
                                    break
                    elif (typ >> 4) == 3:
                        v = [-1, -1, -1]
                        t = [-1, -1, -1]
                        n = [-1, -1, -1]
                        for i in range(0, 3):
                            chunk, v[i] = getidx(chunk, head['vi_s'])
                            if typ & 1:
                                chunk, t[i] = getidx(chunk, head['ti_s'])
                            if typ & 2:
                                chunk, n[i] = getidx(chunk, head['vi_s'])
                        faces.append({'m':m, 'v':v, 't':t, 'n':n})
                    else:
                        print("Only triangles supported")
                        break

            # shapes
            elif magic == b'SHPE':
                chunk, s = getidx(chunk, head['si_s'])
                # TODO
                print("shape ", strs[s])

            # annotation labels
            elif magic == b'LBLS':
                chunk, n = getidx(chunk, head['si_s'])
                chunk, l = getidx(chunk, head['si_s'])
                chunk, c = getidx(chunk, head['ci_s'])
                if ci_s < 4 and c != -1:
                    c = cmap[c]
                elif ci_s == 4:
                    c = [c[0:1] / 255.0, c[1:2] / 255.0, c[2:3] / 255.0, c[3:4] / 255.0]
                # TODO
                print("Labels layer:", strs[n], ", lang:", strs[l], ", color ", c)

            # armature bones and vertex groups
            elif magic == b'BONE':
                chunk, b = getidx(chunk, head['bi_s'])
                chunk, s = getidx(chunk, head['sk_s'])
                # TODO
                print("Skeleton ", b, "bone(s),", s, "skin record(s) (unique bone/weight combos)")

            # animation and timeline marker
            elif magic == b'ACTN':
                chunk, s = getidx(chunk, head['si_s'])
                chunk, f = getidx(chunk, 1)
                chunk, l = getidx(chunk, 2)
                # TODO
                print("Action ", strs[s], ", durationmsec", l, ", numframes ", f)

            # inlined asset
            elif magic == b'ASET':
                chunk, s = getidx(chunk, head['si_s'])
                # TODO
                print("Inlined asset ", strs[s], "(", len(chunk), " bytes)")

            else:
                print("Unknown chunk '%s' skipping..." % (str(magic,'utf-8')))

            data = data[length:]
        del strs
        del cmap

        # ----------------- Start of Blender Specific Stuff ---------------------
        print("\nhead ", head)
        print("\ntmap ", tmap)            # texture map, array of [u, v]
        print("\nvrts ", vrts)            # vertex list, array of [x, y, z, w, [r,g,b,a], skinid]
        print("\nmaterials", materials)   # array of [name, array of [principledBSDFpropname, value]]
        print("\nfaces ", faces)          # triangles, array of [m:materialidx, v[3]:vertexidx, t[3]:tmapidx, n[3]:normalvertexidx]
        print("\nshapes ", shapes)
        print("\nlabels ", labels)
        print("\nbones ", bones)
        print("\nskins ", skins)
        # TODO: add to bpy
        # ----------------- End of Blender Specific Stuff ---------------------

        report({"ERROR"}, "Model 3D importer not fully implemented yet.")

    return {'FINISHED'}


# -----------------------------------------------------------------------------
# Construct and save a Model 3D file
def write_m3d(context,
         filepath,
         report,
         *,
         use_name='',
         use_license='MIT',
         use_author='',
         use_comment='',
         use_scale=1.0,
         use_selection=True,
         use_mesh_modifiers=True,
         use_normals=False,
         use_uvs=True,
         use_colors=True,
         use_shapes=False,
         use_materials=True,
         use_skeleton=True,
         use_animation=True,
         use_fps=25,
         use_quality=0,
         use_inline=False,
         use_compress=True,
         global_matrix=None,
         check_existing=True,
         ):

    # convert string to name identifier
    def safestr(name, morelines=0):
        if name is None:
            return ''
        elif morelines == 3:
            return name.replace('\r', '').strip()
        elif morelines == 2:
            return name.replace('\r', '').replace('\n', ' ').strip()
        elif morelines == 1:
            return name.replace('\r', '').replace('\n', '\r\n').strip()
        else:
            return name.replace(' ', '_').replace('/', '_').replace('\\', '_').replace('\r', '').replace('\n', ' ').strip()

    # set is unique, but has no index, list has index, but not unique...
    def uniquelist(l, e):
        try:
            i = l.index(e)
        except ValueError:
            i = len(l)
            l.append(e)
        return i

    # get index size (we use -1 and -2 as special indices)
    def idxsize(cnt):
        if cnt == 0:
            return 3
        elif cnt < 254:
            return 0
        elif cnt < 65534:
            return 1
        return 2

    # write out an index
    def addidx(fmt, idx):
        # we rely on the fact that in C -1 is a full binary 1 which
        # gives the maximum unsigned value regardless to size, but
        # pack stops us from taking advantage of that
        if fmt == 0:
            if idx < 0:
                idx = 256 + idx
            return pack("<B", idx)
        elif fmt == 1:
            if idx < 0:
                idx = 65536 + idx
            return pack("<H", idx)
        elif fmt == 2:
            if idx < 0:
                idx = 4294967296 + idx
            return pack("<I", idx)
        return b''

    # eliminate minus zero
    def vert(x,y,z,w):
        if x == -0.0:
            x = 0.0
        if y == -0.0:
            y = 0.0
        if z == -0.0:
            z = 0.0
        if w == -0.0:
            w = 0.0
        return x,y,z,w

    with ProgressReport(context.window_manager) as progress:

        if global_matrix is None:
            global_matrix = axis_conversion(to_forward='Z', to_up='Y').to_4x4()
        if use_animation:
            use_skeleton = True
        if use_fps < 1 or use_fps > 120:
            use_fps = 1
        if use_quality == 3:
            digits = 15
        else:
            digits = 4

        # Get Blender objects to export
        depsgraph = context.evaluated_depsgraph_get()
        scene = context.scene
        if use_selection:
            objects = context.selected_objects
        else:
            objects = context.scene.objects

        # Build global lists with unique elements
        cmap = []       # color map entries
        strs = []       # string table with unique strings
        verts = []      # unique list of vertices
        tmaps = []      # texture map UV coordinates
        faces = []      # triangles list
        shapes = []     # shapes list
        labels = []     # annotation labels
        materials = []  # translated material name and properties
        bones = []      # bind-pose skeleton
        skins = []      # array of bone id / weight combinations per vertex
        actions = []    # animations
        inlined = []    # inlined textures
        extras = []     # extra chunks (engine specific)
        progress.enter_substeps(2 + use_materials + use_skeleton + use_animation)

        # ----------------- Start of Blender Specific Stuff ---------------------
        refmats = []    # unique list of referenced Blender material objects
        nb_m = 0        # maximum number of bone weights per vertex
        fi_m = 0        # frame index maximum

        ### Armature ###
        armoffs = 0
        if use_skeleton:
            # this must be done before the mesh so that skin can refer to bones
            progress.step("Exporting Armature")
            for ob_main in objects:
                if ob_main.type != "ARMATURE":
                    continue
                for b in ob_main.data.bones:
                    m = global_matrix @ b.matrix_local
                    a = -1
                    if b.parent:
                        # is there a better way to get the parent's
                        # index in the armature's bone collection?
                        for j,p in enumerate(ob_main.data.bones):
                            if p == b.parent:
                                a = j + armoffs
                                break;
                        p = global_matrix @ b.parent.matrix_local
                        m = p.inverted() @ m
                    # For the top level bones, we need model-space p,q
                    # for the children parent relative p,q
                    p = m.to_translation()  # position
                    q = m.to_quaternion()   # orientation
                    q.normalize()
                    n = safestr(b.name)
                    try:
                        strs.index(n)
                        report({"ERROR"}, "Bone name " + b.name + " (" + n + ") not unique.")
                        use_skeleton = False
                        bones = []
                        use_animation = False
                        break
                    except:
                        pass
                    bones.append([a, uniquelist(strs, n),
                        uniquelist(verts, [vert(
                            round(p[0], digits),
                            round(p[1], digits),
                            round(p[2], digits), 1.0), 0, -1]),
                        uniquelist(verts, [vert(
                            round(q.x, digits),
                            round(q.y, digits),
                            round(q.z, digits),
                            round(q.w, digits)), 0, -2])])
                armoffs = len(bones)

        ### Mesh data ###
        progress.step("Exporting Mesh")
        progress.enter_substeps(len(objects))
        for i, ob_main in enumerate(objects):
            # this mess was taken from io_scene_obj. The point is, at the end we have
            # something that has faces with triangles and model-space coordinate vertices
            if ob_main.parent and ob_main.parent.instance_type in {'VERTS', 'FACES'}:
                continue

            obs = [(ob_main, ob_main.matrix_world)]
            if ob_main.is_instancer:
                obs += [(dup.instance_object.original, dup.matrix_world.copy())
                        for dup in depsgraph.object_instances
                        if dup.parent and dup.parent.original == ob_main]
            for ob, ob_mat in obs:
                # get a copy of the mesh object
                try:
                    o = ob.evaluated_get(depsgraph) if use_mesh_modifiers else ob.original
                    me = o.to_mesh()
                except:
                    me = None
                if me is None or len(me.polygons) < 1:
                    continue
                if use_name is None or use_name == '':
                    use_name = ob.name

                # triangulate if we must
                if len(me.polygons[0].loop_indices) != 3:
                    bm = bmesh.new()
                    bm.from_mesh(me)
                    bmesh.ops.triangulate(bm, faces=bm.faces)
                    bm.to_mesh(me)
                    bm.free()

                # transform vertices to model-space
                me.transform(global_matrix @ ob_mat)
                if ob_mat.determinant() < 0.0:
                    me.flip_normals()
                if use_normals:
                    me.calc_normals_split()

                if use_skeleton and len(ob.vertex_groups) > 0:
                    vg = ob.vertex_groups
                else:
                    vg = []

                if use_uvs and len(me.uv_layers) > 0:
                    uv_layer = me.uv_layers.active.data[:]
                else:
                    uv_layer = []

                if use_colors and len(me.vertex_colors) > 0 and len(me.vertex_colors[me.vertex_colors.active_index].data) > 0:
                    vcol = me.vertex_colors[me.vertex_colors.active_index].data
                else:
                    vcol = []

                matnames = []
                if use_materials:
                    for m in me.materials[:]:
                        if m and m.name:
                            matnames.append(uniquelist(strs, safestr(m.name)))
                        else:
                            matnames.append(-1)

                # Ahhh finally we can get the vertices and faces
                for poly in me.polygons:
                    face = [ -1, [-1,-1,-1], [-1,-1,-1], [-1,-1,-1] ]
                    if len(matnames) > 0:
                        face[0] = matnames[poly.material_index]
                        uniquelist(refmats, me.materials[poly.material_index])
                    for i, li in enumerate(poly.loop_indices):
                        if len(vcol) > 0:
                            c = uniquelist(cmap, [vcol[li].color[0], vcol[li].color[1], vcol[li].color[2], vcol[li].color[3]])
                        else:
                            c = 0
                        v = me.vertices[poly.vertices[i]]
                        if len(vg) > 0 and len(v.groups) > 0:
                            skin = []
                            w = 0.0
                            for g in v.groups:
                                # not sure vertex group index aligns with bone index
                                # skin.append([g.group, round(g.weight, 2)])
                                n = safestr(vg[g.group].name)
                                try:
                                    ni = strs.index(n)
                                except:
                                    report({"ERROR"}, "Vertex group name " + b.name + " (" + n + ") does not match any bone.")
                                    use_skeleton = False
                                    use_animation = False
                                    vg = []
                                    break
                                # no point in bigger precision because we're gonna rescale weight to uint8
                                skin.append([ni, round(g.weight, 2)])
                                w = w + round(g.weight, 2)
                            if w != 1.0:
                                for g,s in enumerate(skin):
                                    skin[g][1] = round(skin[g][1] / w, 2)
                            s = uniquelist(skins, skin)
                        else:
                            s = -1
                        face[1][i] = uniquelist(verts, [vert(
                            round(v.co.x, digits),
                            round(v.co.y, digits),
                            round(v.co.z, digits), 1.0), c, s])
                        if use_normals:
                            face[3][i] = uniquelist(verts, [vert(
                                round(v.normal.x, digits),
                                round(v.normal.y, digits),
                                round(v.normal.z, digits), 1.0), 0, -1])
                        if use_uvs and len(uv_layer) > 0:
                            face[2][i] = uniquelist(tmaps, uv_layer[li].uv[:])
                    faces.append(face)
                del me
            progress.step()
        progress.leave_substeps()

        ### Materials ###
        if use_materials:
            progress.step("Exporting Materials")
            progress.enter_substeps(len(refmats))
            for mi, mat in enumerate(refmats):
                mat_wrap = node_shader_utils.PrincipledBSDFWrapper(mat) if mat else None
                if mat_wrap:
                    props = []
                    for key, mat_wrap_key in mat_property_map:
                        if key == 0:
                            # Kd
                            if mat_wrap.alpha != 0.0 and mat_wrap.alpha != 1.0:
                                d = mat_wrap.alpha
                            elif mat_wrap.base_color and len(mat_wrap.base_color) > 3:
                                d = mat_wrap.base_color[3]
                            else:
                                d = 0.0
                            if d != 0.0:
                                props.append([0, uniquelist(cmap, [mat_wrap.base_color[0], mat_wrap.base_color[1], mat_wrap.base_color[2], d])])
                        elif key == 8:
                            # il
                            il = 0
                            if mat_wrap.specular == 0:
                                il = 1
                            elif mat_wrap.metallic != 0.0:
                                if d != 1.0:
                                    il = 6
                                else:
                                    il = 3
                            elif d != 1.0:
                                il = 9
                            else:
                                il = 2
                            if il != 0:
                                props.append([8, il])
                        elif mat_wrap_key[0][0:2] == "//":
                            continue

                        try:
                            val = getattr(mat_wrap, mat_wrap_key[1], None)
                        except:
                            continue
                        if val is None:
                            continue

                        if key >= 128:
                            if val.image is None:
                                continue
                            imgpath = repr(os.path.basename(val.image.filepath))[1:-1]
                            imgext = os.path.splitext(imgpath)
                            if imgext[1] == "png" or imgext[1] == "PNG":
                                imgpath = imgext[0]
                            s = uniquelist(strs, imgpath)
                            props.append([key, s])
                            if use_inline:
                                try:
                                    data = open(os.path.join(os.path.dirname(filepath), val.image.filepath), 'rb').read()
                                except:
                                    data = b''
                                if len(data) < 8 or data[0:4] != b'\x89PNG':
                                    report({"ERROR"}, "Texture file " + val.image.filepath + " not found or not a valid PNG. Cannot be inlined.")
                                else:
                                    inlined = uniquelist(inlined, [s, data])
                        elif mat_wrap_key[0] == "gscale" and val != 0.0:
                            props.append([key, uniquelist(cmap, [val, val, val, 1.0])])
                        elif mat_wrap_key[0] == "color" and len(val) == 3:
                            props.append([key, uniquelist(cmap, [val[0], val[1], val[2], 1.0])])
                        elif mat_wrap_key[0] == "color" and len(val) == 4:
                            props.append([key, uniquelist(cmap, val)])
                        elif mat_wrap_key[0] == "float" and val != 0.0:
                            props.append([key, val])

                    # append material if it has at least one property
                    if len(props) > 0:
                        materials.append([uniquelist(strs, safestr(mat.name)), props])
                progress.step()
            progress.leave_substeps()

        ### Actions ###
        if use_animation:
            progress.step("Exporting Animations")
            if len(bones) > 0:
                orig_frame = scene.frame_current
                mpf = 1000.0/use_fps # msec per frame
                # collect actions from timeline markers, tricky thing
                # to do because they might be in start-end pairs
                acts = []
                if len(scene.timeline_markers) > 0:
                    nf = 0 # number of total frames
                    tlm = sorted(scene.timeline_markers, key=lambda tl: tl.frame)
                    for i,t in enumerate(tlm):
                        if t.name[-4:] == "_END":
                            continue
                        if i + 1 >= len(tlm):
                            et = scene.frame_end
                        else:
                            et = tlm[i+1].frame
                            if tlm[i+1].name[-4:] != "_END":
                                et = et - 1
                        if et > t.frame:
                            if t.name[-6:] == "_START":
                                n = t.name[:-6]
                            else:
                                n = t.name
                            acts.append([safestr(n), t.frame, et])
                            nf = nf + et - t.frame
                    del tlm
                else:
                    # no markers, one big happy animation only
                    acts.append(["Anim", scene.frame_start, scene.frame_end])
                    nf = scene.frame_end - scene.frame_start
                # ok, now 'acts' is an array of [action name, start frame, end frame]
                progress.enter_substeps(nf + 1)
                for a in acts:
                    lf = 0
                    frames = []     # collect frame with changed bones for this action
                    # we assume that data.bones and pose.bones has the same amount of elements and that
                    # they are in the same order. If this isn't true, then we must match bone names...
                    lastpose = []   # fill up with bind pose on start
                    for b in bones:
                        lastpose.append([b[2], b[3]])
                    # iterate through each frame, and set anim pose for the armature
                    for frame in range(a[1], a[2] + 1):
                        scene.frame_set(frame, subframe=0.0)
                        # walk through the bones in anim pose, collect which one changed
                        changed = []
                        for ob_main in objects:
                            if ob_main.type != "ARMATURE":
                                continue
                            for i, b in enumerate(ob_main.pose.bones):
                                # FIXME: is matrix relative to parent bone already? Or should
                                # we use location and rotation_quaternion? Doc is unclear...
                                # we need model-space p,q only for bones without parents
                                m = global_matrix @ b.matrix
                                if b.parent:
                                    p = global_matrix @ b.parent.matrix
                                    m = p.inverted() @ m
                                p = m.to_translation()
                                q = m.to_quaternion()
                                q.normalize()
                                # differerent?
                                pos = uniquelist(verts, [vert(
                                        round(p[0], digits),
                                        round(p[1], digits),
                                        round(p[2], digits), 1.0), 0, -1])
                                ori = uniquelist(verts, [vert(
                                        round(q.x, digits),
                                        round(q.y, digits),
                                        round(q.z, digits),
                                        round(q.w, digits)), 0, -2])
                                if lastpose[i][0] != pos or lastpose[i][1] != ori:
                                    changed.append([i, pos, ori])
                                    lastpose[i][0] = pos
                                    lastpose[i][1] = ori
                        # do we have changed bones on this frame?
                        if len(changed) > 0:
                            if len(frames) < 1:
                                a[1] = frame
                            frames.append([int((frame-a[1]) * mpf), changed])
                            lf = frame
                            if len(changed) > fi_m:
                                fi_m = len(changed)
                        progress.step()
                    # if the action has at least one frame, save it
                    if len(frames) > 0:
                        actions.append([uniquelist(strs, safestr(a[0])), int((lf-a[1]+1) * mpf), frames])
                scene.frame_set(orig_frame, subframe=0.0)
                progress.leave_substeps()
            else:
                report({"ERROR"}, "Trying to export animations without armature")
        # ----------------- End of Blender Specific Stuff ---------------------

        # Now we should have:
        #  cmap = array of [r, g, b, a]
        #  strs = array of unique strings
        #  verts = array of [x, y, z, w, color, skinid]
        #  tmaps = array of [u, v]
        #  faces = array of [material strid, [3] vertexids, [3] normalvertexids, [3] tmapids }
        #  shapes =
        #  labels =
        #  materials = array of [material strid, array of [property type, property value]]
        #  bones = array of [parent, name strid, pos vertexid, ori vertexid]
        #  skins = array of [[boneid, weight] * 8]
        #  actions = array of [action name strid, durationmsec, array of animation frames]
        #    anim frame = [timestampmsec, array of [boneid, pos vertexid, ori vertexid]]
        #  inlined = array of [name strid, bytes data]
        #  extras = array of [bytes[4] magic, bytes data]

        #print("----------------------------------------------")
        #print(cmap)
        #print(strs)
        #print(verts)
        #print(tmaps)
        #print(faces)
        #print(shapes)
        #print(labels)
        #print(materials)
        #print(bones)
        #print(skins)
        #print(actions)
        #print("----------------------------------------------")

        # normalize coordinates
        min_x = min_y = min_z = 1e10
        max_x = max_y = max_z = -1e10
        for v in verts:
            if v[0] < min_x:
                min_x = v[0]
            if v[0] > max_x:
                max_x = v[0]
            if v[1] < min_y:
                min_y = v[1]
            if v[1] > max_y:
                max_y = v[1]
            if v[2] < min_z:
                min_z = v[2]
            if v[2] > max_z:
                max_z = v[2]
        s = max(abs(min_x), abs(max_x), abs(min_y), abs(max_y), abs(min_z), abs(max_z))
        if s != 1.0 and s != 0.0:
            for i,v in enumerate(verts):
                if verts[i][4] != -2:
                    verts[i][0] = round(verts[i][0] / s, digits)
                    verts[i][1] = round(verts[i][1] / s, digits)
                    verts[i][2] = round(verts[i][2] / s, digits)

        # Construct chunks buffer from lists
        progress.step("Compressing output")

        # create string table and calculate string offsets
        if use_author is None or use_author == "":
            use_author = getenv("LOGNAME", "")
        stridx = [0] * (len(strs))
        st = bytes(safestr(use_name, 2), 'utf-8') + pack("<b", 0)
        st = st + bytes(safestr(use_license, 2), 'utf-8') + pack("<b", 0)
        st = st + bytes(safestr(use_author, 2), 'utf-8') + pack("<b", 0)
        st = st + bytes(safestr(use_comment, 1), 'utf-8') + pack("<b", 0)
        o = len(st)
        for i, s in enumerate(strs):
            s = bytes(s, 'utf-8') + pack("<b", 0)
            st = st + s
            stridx[i] = o
            o = o + len(s)

        # construct model header chunk
        ci_s = idxsize(len(cmap))
        ti_s = idxsize(len(tmaps))
        vi_s = idxsize(len(verts))
        si_s = idxsize(o)
        bi_s = idxsize(len(bones))
        sk_s = idxsize(len(skins))
        hi_s = idxsize(len(shapes))
        fi_s = idxsize(len(faces))
        if nb_m < 2:
            nb_s = 1
        elif nb_m == 2:
            nb_s = 2
        elif nb_m <= 4:
            nb_s = 4
        else:
            nb_s = 8
        fc_s = idxsize(fi_m)
        flags = (use_quality << 0) | (vi_s << 2) | (si_s << 4) | (ci_s << 6) | (ti_s << 8) | (bi_s << 10) | (nb_s << 12)
        flags |= (sk_s << 14) | (fc_s << 16) | (hi_s << 18) | (fi_s << 20)
        buf = pack("<f", use_scale) + pack("<I", flags) + st
        buf = b'HEAD' + pack("<I",len(buf) + 8) + buf

        # color map
        if len(cmap) > 0 and ci_s < 4:
            buf = buf + b'CMAP' + pack("<I", len(cmap) * 4 + 8)
            for col in cmap:
                for i in range(0, 4):
                    buf = buf + pack("<B", int(col[i] * 255))

        # texture map
        if len(tmaps) > 0:
            buf = buf + b'TMAP' + pack("<I", len(tmaps) * 2 * (1 << use_quality) + 8)
            for t in tmaps:
                if use_quality == 0:
                    buf = buf + pack("<BB", int(t[0] * 255), int(t[1] * 255))
                elif use_quality == 1:
                    buf = buf + pack("<HH", int(t[0] * 65535), int(t[1] * 65535))
                elif use_quality == 3:
                    buf = buf + pack("<dd", t[0], t[1])
                else:
                    buf = buf + pack("<ff", t[0], t[1])

        # vertex list
        if len(verts) > 0:
            o = b''
            for v in verts:
                for i in range(0, 4):
                    if use_quality == 0:
                        o = o + pack("<b", int(v[i] * 127))
                    elif use_quality == 1:
                        o = o + pack("<h", int(v[i] * 32767))
                    elif use_quality == 3:
                        o = o + pack("<d", v[i])
                    else:
                        o = o + pack("<f", v[i])
                if ci_s < 4:
                    o = o + addidx(ci_s, v[4])
                else:
                    o = o + pack("<I", cmap[v[4]])
                o = o + addidx(sk_s, v[5])
            buf = buf + b'VRTS' + pack("<I", len(o) + 8) + o

        # skeleton
        if len(bones) > 0 or len(skins) > 0:
            o = addidx(bi_s, len(bones)) + addidx(sk_s, len(skins))
            for b in bones:
                o = o + addidx(bi_s, b[0]) + addidx(si_s, stridx[b[1]]) + addidx(vi_s, b[2]) + addidx(vi_s, b[3])
            for s in skins:
                if nb_s > 1:
                    for i in range(0, 1 << nb_s):
                        if i >= len(s):
                            o = o + pack("<B", 0)
                        else:
                            o = o + pack("<B", int(s[i][1] * 255))
                for i in range(0, min(len(s)+1, 1 << nb_s)):
                    if s[i][1] != 0.0:
                        o = o + addidx(bi_s, s[i][0])
            buf = buf + b'BONE' + pack("<I", len(o) + 8) + o

        # materials
        if len(materials) > 0:
            for m in materials:
                o = addidx(si_s, stridx[m[0]])
                for p in m[1]:
                    o = o + pack("<B", p[0])
                    t = mat_property_map[p[0]]
                    if t[0] == "color" or t[0] == "gscale":
                        if ci_s < 4:
                            o = o + addidx(ci_s, p[1])
                        else:
                            o = o + pack("<I", cmap[p[1]])
                    elif t[0] == "byte" or t[0] == "//byte":
                        o = o + pack("<B", p[1])
                    elif p[0] >= 128:
                        o = o + addidx(si_s, stridx[p[1]])
                    else:
                        o = o + pack("<f", p[1])
                buf = buf + b'MTRL' + pack("<I", len(o) + 8) + o

        # triangle mesh
        if len(faces) > 0:
            l = -1
            o = b''
            for f in faces:
                if l != f[0]:
                    l = f[0]
                    o = o + pack("<b", 0) + addidx(si_s, stridx[l])
                o = o + pack("<b", (len(f[1]) << 4) | (use_uvs) | (use_normals << 1))
                for i,v in enumerate(f[1]):
                    o = o + addidx(vi_s, v)
                    if use_uvs:
                        o = o + addidx(ti_s, f[2][i])
                    if use_normals:
                        o = o + addidx(ti_s, f[3][i])
            buf = buf + b'MESH' + pack("<I", len(o) + 8) + o

        # shapes
        if len(shapes) > 0:
            l = -1
            o = b''
            for f in shapes:
                o = o + b''
            buf = buf + b'SHPE' + pack("<I", len(o) + 8) + o

        # labels
        if len(labels) > 0:
            l = -1
            o = b''
            for f in labels:
                o = o + b''
            buf = buf + b'LBLS' + pack("<I", len(o) + 8) + o

        # actions (animations)
        if len(actions) > 0:
            for a in actions:
                if len(a[2]) < 1:
                    continue
                o = addidx(si_s, stridx[a[0]]) + pack("<H", len(a[2])) + pack("<I", a[1])
                for f in a[2]:
                    o = o + pack("<I", f[0]) + addidx(fc_s, len(f[1]))
                    for t in f[1]:
                        o = o + addidx(bi_s, t[0]) + addidx(vi_s, t[1]) + addidx(vi_s, t[2])
                buf = buf + b'ACTN' + pack("<I", len(o) + 8) + o

        # inlined assets
        if len(inlined) > 0:
            for i in inlined:
                o = addidx(si_s, stridx[i[0]]) + i[1]
                buf = buf + b'ASET' + pack("<I", len(o) + 8) + o

        # extra chunks
        if len(extras) > 0:
            for e in extras:
                buf = buf + e[0][0:3] + pack("<I", len(e[1]) + 8) + e[1]

        # End chunk
        buf = buf + b'OMD3';
        if use_compress:
            import zlib
            buf = zlib.compress(buf, 9)

        # add file header and write out file
        f = open(filepath, 'wb')
        s = len(buf) + 8
        f.write(b'3DMO' + pack("<L", s) + buf)
        f.close()

        progress.leave_substeps("Finished!")

        report({"INFO"}, "Model 3D " + filepath + " (" + str(s) + " bytes) exported.")
    return {'FINISHED'}


# -----------------------------------------------------------------------------
# Blender integration
import bpy
from bpy.props import (
        BoolProperty,
        FloatProperty,
        StringProperty,
        IntProperty,
        EnumProperty,
        )
from bpy_extras.io_utils import (
        ExportHelper,
        ImportHelper,
        axis_conversion,
        )

class ImportM3D(bpy.types.Operator, ImportHelper):
    """Load a Model 3D File (.m3d)"""

    bl_idname = "import_scene.m3d"
    bl_label = 'Import M3D'
    bl_options = {'PRESET'}

    filename_ext = ".m3d"
    filter_glob: StringProperty(
            default="*.m3d;*.a3d",
            options={'HIDDEN'},
            )


    def execute(self, context):
        return read_m3d(context, self.filepath, self.report)


class ExportM3D(bpy.types.Operator, ExportHelper):
    """Save a Model 3D File (.m3d)"""

    bl_idname = "export_scene.m3d"
    bl_label = 'Export M3D'
    bl_options = {'PRESET'}

    filename_ext = ".m3d"
    filter_glob: StringProperty(
            default="*.m3d",
            options={'HIDDEN'},
            )

    # model properties
    use_name: StringProperty(
            name="Model Name",
            description="Name of the exported model",
            default="",
            )
    use_license: StringProperty(
            name="License",
            description="Licensing, copyright notice",
            default="MIT",
            )
    use_author: StringProperty(
            name="Author",
            description="Your name and contact (email, git repo url etc.)",
            default="",
            )
    use_comment: StringProperty(
            name="Comment",
            description="Any description or comment on the model",
            default="",
            )
    use_scale: FloatProperty(
            name="Scale (meter)",
            description="Specify the size of the model's bounding box in SI meters",
            min=0.0, max=1000.0,
            default=1.0,
            )
    # import range
    use_selection: BoolProperty(
            name="Selection Only",
            description="Export selected objects only",
            default=False,
            )
    use_mesh_modifiers: BoolProperty(
            name="Apply Modifiers",
            description="Apply modifiers",
            default=True,
            )
    # export properties
    use_normals: BoolProperty(
            name="Include Normals",
            description="Export one normal per vertex and per face, to represent flat faces and sharp edges",
            default=False,
            )
    use_uvs: BoolProperty(
            name="Include UVs",
            description="Write out the active UV coordinates",
            default=True,
            )
    use_colors: BoolProperty(
            name="Include Vertex Colors",
            description="Write out individual vertex colors (independent to material colors)",
            default=True,
            )
    use_shapes: BoolProperty(
            name="Write Shapes",
            description="Write out as parameterized shapes",
            default=False,
            )
    use_materials: BoolProperty(
            name="Write Materials",
            description="Write out the materials",
            default=True,
            )
    use_skeleton: BoolProperty(
            name="Write Armature",
            description="Write out armature (bones hiearachy and skin)",
            default=True,
            )
    use_animation: BoolProperty(
            name="Write Animation",
            description="Write out actions (implies armature)",
            default=True,
            )
    use_fps: IntProperty(
            name="FPS",
            description="Specify frame per second. Blender only nows about frames",
            min=1, max=120,
            default=25,
            )
    use_quality_sel: EnumProperty(
            name="Coordinate Precision",
            items=(('0', '8 bits (int8)', '1/256 coordinate unit (default)'),
                   ('1','16 bits (int16)', '1/65536 coordinate unit (more than enough)'),
                   ('2','32 bits (float)', 'float precision coordinates (used by most other binary formats)'),
                   ('3','64 bits (double)', 'double precision coordinates (rarely needed)'),
                  ),
            description="Coordinate grid system's size and precision",
            default='0',
            )
    use_inline: BoolProperty(
            name="Embed Assets",
            description="Inline assets (like textures) into output, create a single file that contains everything",
            default=False,
            )
    use_compress: BoolProperty(
            name="Use Compression",
            description="Use deflate on binary data. Unless you're writing your own M3D parser, keep it checked",
            default=True,
            )


    def execute(self, context):
        # Exit edit mode before exporting, so current object states are exported properly.
        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT')

        self.use_quality = int(self.use_quality_sel)
        keywords = self.as_keywords(ignore=("filepath", "filter_glob", "use_quality_sel"))
        return write_m3d(context, self.filepath, self.report, **keywords)


def menu_func_export(self, context):
    self.layout.operator(ExportM3D.bl_idname, text="Model 3D (.m3d)")


def menu_func_import(self, context):
    self.layout.operator(ImportM3D.bl_idname, text="Model 3D (.m3d)")


def register():
    bpy.utils.register_class(ExportM3D)
    bpy.utils.register_class(ImportM3D)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
#    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister():
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
#    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.utils.unregister_class(ExportM3D)
    bpy.utils.unregister_class(ImportM3D)


if __name__ == "__main__":
    register()
