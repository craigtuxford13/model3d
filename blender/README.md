Model 3D Blender Integration
============================

This Python script adds Model 3D import / export option to [Blender](https://blender.org) 2.80 and above.

Installation
------------

1. Download [io_scene_m3d.py](https://gitlab.com/bztsrc/model3d/blob/master/blender/io_scene_m3d.py)
2. Start Blender
3. Go to menu, "Edit" > "Preferences..." > "Add-ons" tab > "Install..." button
4. Browse and double click on the downloaded file
5. On the "Add-ons" tab, search for "m3d"
6. Click on "Enable Add-on."
7. Now in the menu, "File" > "Export" > "Model 3D (.m3d)" and "File" > "Import" > "Model 3D (.m3d)" should appear

Export
------

Features: vertex list (with colors), mesh data, normals, UVs, materials (with colors, textures, standard and PBR properties),
bone hierarchy (armature), skin (bone weights per vertex), inlined assets, actions (animations).

### Tips and Tricks

Create exatly one Armature. Bones that are right beneath the Armature are stored with model-space coordinates. Bones under other
bones however are stored with relative positions to their parent, allowing a much compact export output. So you should create only
one root bone node under the Armature (eg.: "spine"), and create the entire skeleton structure under that single root bone node.

Don't use spaces ' ', slash '/' or backslash '\\' in bone names (everyting else is okay, like dot '.', hash '#' etc.). Create
exactly one vertex group per bone, and name it exactly as the bone. This group should reference all verticies that the bone
influences.

By default, the animation is exported as one single big action, but you can export several smaller actions instead. For that,
you have to create timeline markers on the "Animation" tab. You have two options:
1. you simply place a marker on action start frames, and you name the marker as "(action)"
2. you specify intervals with double markers, named "(action)_START" and "(action)_END".
If you choose the second variant, then only frames inside one of the intervals will be exported.

The plugin is written in a way that it first collects data from bpy into lists. Then the second half serializes those lists into
M3D chunks. To minimize memory consumption and to speed up the export, the lists contain indices to other lists mostly. They are
documented in the plugin's source. If the Blender API changes (again), then only the list filler part needs to be changed.

Import
------

`WARNING: this part of the plugin is Work In Progress`

Features: it can read a M3D file, inflates it, parses the chunks in it, but just prints the chunks, does not add data to bpy
structures for real yet.

Missing: almost everything.

